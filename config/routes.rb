Rails.application.routes.draw do
	resources :products
	resources :categories
	root to: 'home#index'

	devise_for :users
	# For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

	mount ActionCable.server => '/cable'
end
