class ProductBroadcastJob < ApplicationJob
	queue_as :default

	def perform(product)
		product.reload
		ActionCable.server.broadcast "products_channel", content: render_product(product), product: product
	end

	private

	def render_product(product)
		ProductsController.render partial: 'product', locals: {product: product}
	end
end
