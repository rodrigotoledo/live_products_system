# actions for all products in home page
jQuery(document).on 'turbolinks:load', ->

  App.global_products = App.cable.subscriptions.create {
      channel: "ProductChannel"
    },
    connected: ->

    disconnected: ->

    received: (data) ->
      if !data.product.active
        $('#product_box_'+data.product.id).remove()
      else if $('#product_box_'+data.product.id).length > 0
        $('#product_box_'+data.product.id).html(data.content)


    update_box: (product_id) ->
      @perform 'update_box', product_id: product_id

  $('#reload_product').click (e) ->
    $this = $(this)
    App.global_products.update_box $this.data('product-id')
    e.preventDefault()
    return false
