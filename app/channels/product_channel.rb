class ProductChannel < ApplicationCable::Channel

	def subscribed
    stream_from "products_channel"
  end

	def update_box(product_info)
		logger.info("----------------")
		logger.info(product_info.inspect)
		ProductBroadcastJob.perform_later(Product.find(product_info["product_id"]))
	end
end
