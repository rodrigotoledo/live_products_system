class Product < ApplicationRecord
	after_save { ProductBroadcastJob.perform_later(self) }

	belongs_to :category

	has_attached_file :photo, styles: { medium: "300x300>", thumb: "200x200>" }

	validates_attachment_content_type :photo, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif", "application/pdf"]

	def label
		if quantity <= 3
			I18n.t(:label_danger)
		elsif quantity <= 7
			I18n.t(:label_warning)
		else
			I18n.t(:label_success)
		end
	end

	def status_label
		active? ? I18n.t(:in_stock) : (:out_of_stock)
	end
end
