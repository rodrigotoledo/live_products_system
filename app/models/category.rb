class Category < ApplicationRecord
	has_many :products, autosave: true
end
