['Eletronics', 'Softwares', 'Services'].each_with_index do |name, product_index|
	category = Category.new(name: name)
	8.times.each do |i|
		category.products.build(name: "#{name} - #{i}", quantity: rand(1..10),
			photo: File.new(Rails.root.join("db/product_images/#{product_index}.jpg").to_s))
	end
	category.save!
end
