class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.references :category, foreign_key: true
      t.string :name
      t.integer :quantity, default: 10
      t.boolean :active, default: true

      t.timestamps
    end
  end
end
