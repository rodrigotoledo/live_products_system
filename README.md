Objetivos

- Central administrativa com Devise
  - Cadastro de Produtos
    - Usuários visualizando os produtos
  - Cadastro de Usuários
  - Usuários online
  - Carrinhos de Compras Ativos

- Frontend
  - Lista de Produtos removendo quando os mesmos forem desativados
  - Detalhes do Produto com sua Quantidade em estoque
  - Redirecionar para Home quando o Produto for desativado
  - Redicionar para Home, avisar Usuário que o Produto foi desativado do carrinho e remover automaticamente do carrinho
